function sendAuthorizeJSON() {
$.ajax({
   url: 'http://localhost:9800/getAllUserFiles',
   type: 'GET',
   contentType: 'application/json'
   headers: {
      'Authorization': 'Bearer ' + $window.sessionStorage.accessToken
   },
   success: function (result) {
       // CallBack(result);
   },
   error: function (error) {

   }
});


  (function() {
    var files = [
      {
        "id": 1,
        "fileName": "Shchur CV.pdf",
        "fileUri": "http://localhost:9800/downloadFile/Shchur%20CV.pdf",
        "fileDirection": "E:\\CourseWork2\\Files\\Shchur CV.pdf"
      },
      {
        "id": 2,
        "fileName": "IH_MkIII_Armour.jpg",
        "fileUri": "http://localhost:9800/downloadFile/IH_MkIII_Armour.jpg",
        "fileDirection": "E:\\CourseWork2\\Files\\IH_MkIII_Armour.jpg"
      }
    ]
    'use-strict';
    var elem,
      dataFn = $('[data-fn="contacts"]'),
      thisUrl = dataFn.data('url');
    if (typeof $.table_of_contacts == 'undefined')
    $.table_of_contacts = {};
    $.table_of_contacts.get = {
    init: function() {
      if(dataFn){
        this.getJson();
      }else{
        dataFn.html('No data found.');
      }
    },
    /* = Get data    ------------------------*/
    getJson: function(url) {
      var self = this;
      // loading data before
      dataFn.html('<span class="loading_table">'+
                  'Loading Please Wait ....'+
                  '</span>');
      // No ajax cache
      $.ajaxSetup({ cache: false });
      // Get json
      $.getJSON(thisUrl,function(data){
        // load template
        var out_html = self.tpl();
        $.each(data,function(i,obj){
          // load inner template
          out_html += self.tpl_inner(obj);
        });
        // close tag
        out_html += '</tbody>';
        // render templates
        dataFn.html(out_html);
        // error
      }).error(function(j,t,e){
        // render error.
        dataFn.html('<span class="error_table">'+
          'Error = '+e+
          '</span>');
      });
    },
    // head table template
    tpl: function(){
      var html = '<thead>'+
        '<tr>'+
        '<th>id</th>'+
        '<th>fileName</th>'+
        '<th>fileUri</th>'+
        '</tr>'+
        '</thead>'+
        '<tbody >';
      return html;
    },
    // inner template
    tpl_inner: function(obj){
      var  html= '<tr>'+
        '<td>'+obj.id+'</td>'+
        '<td>'+obj.fileName+'</td>'+
        '<td>'+
          '<a href="'+obj.fileUri+'" title="'+
          obj.fileUri">'+
          obj.fileUri +
        '</td>'+
        '</tr>';
      return html;
    }
  };
  // on ready render data
  $(document).ready(function() {
    $.table_of_contacts.get.init();
  });
})().call(this);
}
