package courseWork2v3.com.ramoz.prod.cw2.dao;

import courseWork2v3.com.ramoz.prod.cw2.model.File;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface FileDao {
  File addFile(File directory);
  List<File> getAll();
  File getByFileName(String fileName);
  File editFile(File directory);
  List<File> findAllUserFiles(int id);
  void deleteFile(int id);

}
