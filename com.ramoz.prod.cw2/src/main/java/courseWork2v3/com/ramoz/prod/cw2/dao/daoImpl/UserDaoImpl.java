package courseWork2v3.com.ramoz.prod.cw2.dao.daoImpl;

import courseWork2v3.com.ramoz.prod.cw2.model.User;
import courseWork2v3.com.ramoz.prod.cw2.model.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class UserDaoImpl implements
  courseWork2v3.com.ramoz.prod.cw2.dao.UserDao {
  @Autowired
  private UserRepository userRepo;
  private BCryptPasswordEncoder bCryptPasswordEncoder;
  @Autowired
  public UserDaoImpl(UserRepository userRepo,
    BCryptPasswordEncoder bCryptPasswordEncoder) {
    this.userRepo = userRepo;
    this.bCryptPasswordEncoder = bCryptPasswordEncoder;
  }

  @Override
  public User addUser(User user) {
    User savedUser = userRepo.saveAndFlush(user);
    return savedUser;
  }
  @Override
  public User getByUsername(String username) {
    return userRepo.findByUsernameLikeIgnoreCase(username);
  }
  @Override
  public User getByEmail(String email) {
    return userRepo.findByEmail(email);
  }
  @Override
  public User editUser(User user) {
    return userRepo.saveAndFlush(user);
  }
  @Override
  public void deleteUser(long id) {
    userRepo.deleteById(id);
  }
  @Override
  public User findByUsername(String username) {
    return userRepo.findByUsernameLikeIgnoreCase(username);
  }
  public User getById(Integer id) {
    return userRepo.findById(id);
  }
}
