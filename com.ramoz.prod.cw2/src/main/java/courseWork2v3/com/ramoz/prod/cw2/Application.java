package courseWork2v3.com.ramoz.prod.cw2;

import courseWork2v3.com.ramoz.prod.cw2.controller.property.FileStorageProperties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import java.util.List;

@SpringBootApplication
@EnableJpaRepositories("courseWork2v3.com.ramoz.prod.cw2.*")
@ComponentScan(basePackages = { "courseWork2v3.com.ramoz.prod.cw2.*" })
@EntityScan("courseWork2v3.com.ramoz.prod.cw2.*")
@EnableConfigurationProperties({
  FileStorageProperties.class
})
public class Application {
	static Logger logger = LoggerFactory.getLogger(Application.class);
	private static List Directories;
	public static void main(String[] args) {
		ConfigurableApplicationContext context =
			SpringApplication.run(Application.class, args);
		logger.warn("");
		logger.warn("Program initialization completed.");
	}
}
