package courseWork2v3.com.ramoz.prod.cw2.model.repository;

import courseWork2v3.com.ramoz.prod.cw2.model.File;
import courseWork2v3.com.ramoz.prod.cw2.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface FileRepository extends JpaRepository<File, Integer> {
  File findByFileName(@Param("name") String folderName);
  File findById(@Param("id") int id);
  List<File> findByUserId(@Param("id") int id);
}
