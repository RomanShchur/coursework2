package courseWork2v3.com.ramoz.prod.cw2.controller;

import courseWork2v3.com.ramoz.prod.cw2.model.User;
import courseWork2v3.com.ramoz.prod.cw2.dao.daoImpl.UserDaoImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import javax.transaction.Transactional;

@Service
public class MyUserDetailsService implements UserDetailsService {
  @Autowired
  private UserDaoImpl udaoImpl;
  @Override
  @Transactional
  public UserDetails loadUserByUsername(String userName) throws UsernameNotFoundException {
    User user = udaoImpl.getByUsername(userName);
    return buildUserForAuthentication(user);
  }
  private UserDetails buildUserForAuthentication(User user) {
    return new User(user.getUsername(), user.getPassword());
  }
}
