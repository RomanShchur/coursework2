package courseWork2v3.com.ramoz.prod.cw2.model;

public class UserDto {
  private String username;
  private String password;
  private String email;
  public UserDto() {
  }
  public UserDto(String username, String password, String email) {
    this.username = username;
    this.password = password;
    this.email = email;
  }
  public String getUsername() {
    return username;
  }
  public String getPassword() {
    return password;
  }
  public String getEmail() {
    return email;
  }
  public void setUsername(String username) {
    this.username = username;
  }
  public void setPassword(String password) {
    this.password = password;
  }
  public void setEmail(String email) {
    this.email = email;
  }
  @Override public String toString() {
    return "UserDto{" + "username='" + username + '\'' + ", password='"
      + password + '\'' + ", email='" + email + '\'' + '}';
  }
}
