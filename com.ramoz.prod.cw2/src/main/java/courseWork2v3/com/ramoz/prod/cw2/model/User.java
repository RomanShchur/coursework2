package courseWork2v3.com.ramoz.prod.cw2.model;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import java.util.Collection;
import java.util.List;

@Entity
@Table(name = "user")
public class User implements UserDetails {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "id", updatable = false, nullable = false)
  private int id;
  @NotBlank
  @Column(name = "username", nullable = false)
  private String username;
  @NotBlank
  @Column(name = "password", nullable = false)
  private String password;
  @NotBlank
  @Column(name = "email", nullable = false)
  @Email
  private String email;
  @OneToMany(mappedBy = "user")
  private List<File> files;

  public User() {
  }
  public User(String username, String password, String email) {
    this.username = username;
    this.password = password;
    this.email = email;
  }
  public User(String username, String password) {
    this.username = username;
    this.password = password;
  }
  public int getId() {
    return id;
  }
  public String getUsername() {
    return username;
  }
  public String getPassword() {
    return password;
  }
  public String getEmail() {
    return email;
  }
  public void setId(int id) {
    this.id = id;
  }
  public void setUsername(String username) {
    this.username = username;
  }
  public void setPassword(String password) {
    this.password = password;
  }
  public void setEmail(String email) {
    this.email = email;
  }
  @Override public String toString() {
    return "User{" + "id=" + id + ", username='" + username + '\''
      + ", password='" + password + '\'' + ", email='" + email + '\'' + '}';
  }
  @Override public boolean isAccountNonExpired() {
    return true;
  }
  @Override public boolean isAccountNonLocked() {
    return true;
  }
  @Override public boolean isCredentialsNonExpired() {
    return true;
  }
  @Override public boolean isEnabled() {
    return true;
  }
  @Override public Collection<? extends GrantedAuthority> getAuthorities() {
    return null;
  }
}
