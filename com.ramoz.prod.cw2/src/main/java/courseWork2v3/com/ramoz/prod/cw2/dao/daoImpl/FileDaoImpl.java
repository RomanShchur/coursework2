package courseWork2v3.com.ramoz.prod.cw2.dao.daoImpl;

import courseWork2v3.com.ramoz.prod.cw2.dao.FileDao;
import courseWork2v3.com.ramoz.prod.cw2.model.File;
import courseWork2v3.com.ramoz.prod.cw2.model.repository.FileRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;

@Service
public class FileDaoImpl implements FileDao {
  @Autowired
  private FileRepository fileRepo;

  @Override
  public File addFile(File file) {
    File savedDir = fileRepo.saveAndFlush(file);
    return savedDir;
  }
  @Override
  public List<File> getAll() {
    return fileRepo.findAll();
  }
  @Override
  public File getByFileName(String filename) {
    return fileRepo.findByFileName(filename);
  }
  public File getByFileId(int id) {
    return fileRepo.findById(id);
  }
  public void deleteFile(File file) {
    fileRepo.delete(file);
  }
  @Override
  public File editFile(File file) {
    return fileRepo.saveAndFlush(file);
  }
  @Override
  public List<File> findAllUserFiles(int id) {
    return fileRepo.findByUserId(id);
  }
  @Override
  public void deleteFile(int id) {
    fileRepo.deleteById(id);
  }
}
