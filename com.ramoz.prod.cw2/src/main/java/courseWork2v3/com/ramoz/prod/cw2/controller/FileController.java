package courseWork2v3.com.ramoz.prod.cw2.controller;

import courseWork2v3.com.ramoz.prod.cw2.config.webConfig.JwtRequestFilter;
import courseWork2v3.com.ramoz.prod.cw2.controller.service.FileStorageService;
import courseWork2v3.com.ramoz.prod.cw2.dao.daoImpl.FileDaoImpl;
import courseWork2v3.com.ramoz.prod.cw2.dao.daoImpl.UserDaoImpl;
import courseWork2v3.com.ramoz.prod.cw2.model.File;
import courseWork2v3.com.ramoz.prod.cw2.model.UploadFileResponse;
import courseWork2v3.com.ramoz.prod.cw2.model.User;
import courseWork2v3.com.ramoz.prod.cw2.config.webConfig.JwtTokenUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@RestController
public class FileController {
  private static final Logger logger = LoggerFactory.getLogger(FileController.class);
  @Autowired
  private FileStorageService fileStorageService;
  @Autowired
  private FileDaoImpl fileDaoImpl;
  @Autowired
  private UserDaoImpl userDaoImpl;
  @Autowired
  private JwtTokenUtil jwtTokenUtil;
  @Autowired
  private JwtRequestFilter jwtRequestFilter;
  private int userId = 1;
  private java.io.File fileRule;

  @GetMapping("/getAllUserFiles")
  public List<File> getAllUserFiles (@RequestHeader("Authorization") String token, HttpServletRequest request,
    HttpServletResponse response) {
    User owner =
      jwtRequestFilter.checkUserRequestToken(token, request, response);
    return fileDaoImpl.findAllUserFiles(owner.getId());
  }
  @DeleteMapping(value = "/userDeleteFile/{id}")
  public @ResponseBody boolean deleteFile(@PathVariable("id") int id,
    @RequestHeader("Authorization") String token,
    HttpServletRequest request, HttpServletResponse response) {
    jwtRequestFilter.checkUserRequestToken(token, request, response);
    File fileToDelete = fileDaoImpl.getByFileId(id);
    fileDaoImpl.deleteFile(fileToDelete.getId());
    return fileToDelete.deleteFile();
  }
  @PostMapping("/userUploadFile")
  public UploadFileResponse uploadFile(@RequestParam("file") MultipartFile file,
    @RequestHeader("Authorization") String token,
    HttpServletRequest request, HttpServletResponse response) {
    String fileName = fileStorageService.storeFile(file);
    String fileDownloadUri = ServletUriComponentsBuilder.fromCurrentContextPath()
      .path("/downloadFile/")
      .path(fileName)
      .toUriString();
    String fileDirection = "E:\\CourseWork2\\Files\\" + fileName;
    User owner =
      jwtRequestFilter.checkUserRequestToken(token, request, response);
    System.out.println(owner.getUsername());
    File dbFile = new File(fileName, fileDirection, fileDownloadUri, owner);
    System.out.println(dbFile.toString());
    fileDaoImpl.addFile(dbFile);
    return new UploadFileResponse(fileName, fileDownloadUri,
      file.getContentType(), file.getSize());
  }
  @GetMapping("/downloadFile/{fileName:.+}")
  public ResponseEntity<Resource> downloadFile(@PathVariable String fileName, HttpServletRequest request) {
    Resource resource = fileStorageService.loadFileAsResource(fileName);
    String contentType = null;
    try {
      contentType = request.getServletContext().getMimeType(resource.getFile().getAbsolutePath());
    } catch (IOException ex) {
      logger.info("Could not determine file type.");
    }
    if(contentType == null) {
      contentType = "application/octet-stream";
    }
    return ResponseEntity.ok()
      .contentType(MediaType.parseMediaType(contentType))
      .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + resource.getFilename() + "\"")
      .body(resource);
  }
}
