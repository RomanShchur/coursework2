package courseWork2v3.com.ramoz.prod.cw2.dao;

import courseWork2v3.com.ramoz.prod.cw2.model.User;
import org.springframework.stereotype.Service;

@Service
public interface UserDao {
  User addUser(User user);
  User getByUsername(String username);
  User getByEmail(String email);
  User editUser(User user);
  void deleteUser(long id);
  User findByUsername(String username);

}
