package courseWork2v3.com.ramoz.prod.cw2.config.webConfig;

import org.springframework.security.core.userdetails.UserDetailsService;
import courseWork2v3.com.ramoz.prod.cw2.model.User;
import courseWork2v3.com.ramoz.prod.cw2.model.UserDto;
import courseWork2v3.com.ramoz.prod.cw2.model.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

@Component
public class JwtUserDetailsService implements UserDetailsService {
  @Autowired
  private UserRepository userDao;
  @Autowired
  private PasswordEncoder bcryptEncoder;

  @Override
  public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
    User user = this.userDao.findByUsernameLikeIgnoreCase(username);
    if (user == null) {
      throw new UsernameNotFoundException("User not found with username: " + username);
    }
    return new /*org.springframework.security.core.userdetails.*/User(user
    .getUsername(), user.getPassword(), user.getEmail());
  }
  public User save(UserDto inUser) {
    User newUser = new User();
    newUser.setUsername(inUser.getUsername());
    newUser.setPassword(bcryptEncoder.encode(inUser.getPassword()));
    newUser.setEmail(inUser.getEmail());
    return userDao.save(newUser);
  }
}
