package courseWork2v3.com.ramoz.prod.cw2.model;

import javax.persistence.*;

@Entity
@Table(name = "file")
public class File {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "id", updatable = false, nullable = false)
  private int id;
  @Column(name = "file_name", nullable = false)
  private String fileName;
  @Column(name = "file_uri", nullable = false)
  private String fileUri;
  @Column(name = "file_direction")
  private String fileDirection;
  @ManyToOne
  @JoinColumn(name="user_id")
  private User user;

  public File() {
  }
  public File(String fileName, String fileDirection, String fileUri,
    User user) {
    this.fileName = fileName;
    this.fileUri = fileUri;
    this.fileDirection = fileDirection;
    this.user = user;
  }
  public int getId() {
    return id;
  }
  public String getFileName() {
    return fileName;
  }
  public String getFileUri() {
    return fileUri;
  }
  public String getFileDirection() {
    return fileDirection;
  }
  public void setId(int id) {
    this.id = id;
  }
  public void setFolderName(String folderName) {
    this.fileName = folderName;
  }
  public void setFileUri(String fileUri) {
    this.fileUri = fileUri;
  }
  public void setFileDirection(String fileDirection) {
    this.fileDirection = fileDirection;
  }
  @Override public String toString() {
    return "File{" + "id=" + id + ", fileName='" + fileName + '\''
      + ", fileUri='" + fileUri + '\'' + ", fileDirection='" + fileDirection
      + '\'' + ", user=" + user + '}';
  }
  public boolean deleteFile() {
    java.io.File f = new java.io.File(this.getFileDirection());
    return  f.delete();
  }
}
