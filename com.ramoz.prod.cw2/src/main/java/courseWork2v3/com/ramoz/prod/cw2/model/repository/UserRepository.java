package courseWork2v3.com.ramoz.prod.cw2.model.repository;

import courseWork2v3.com.ramoz.prod.cw2.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {
  User findByUsernameLikeIgnoreCase(@Param("username") String username);
  User findByEmail(@Param("email") String email);
  User findById(@Param("id") long id);
  Boolean existsByUsername(String username);
  Boolean existsByEmail(String email);
  Optional<User> findByUsernameOrEmail(String username, String email);
}
