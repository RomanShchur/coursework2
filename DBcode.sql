-- MySQL Workbench Synchronization
-- Generated: 2019-12-05 19:17
-- Model: New Model
-- Version: 1.0
-- Project: Name of the project
-- Author: RamoZ

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';
drop database `mycoursework01`;
create database `mycoursework01`;

use `mycoursework01`;
drop table `user`;
CREATE TABLE IF NOT EXISTS `mycoursework01`.`user` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `username` VARCHAR(60) NOT NULL,
  `password` VARCHAR(255) NOT NULL,
  `email` VARCHAR(60) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

ALTER TABLE `user` ALTER `id` SET DEFAULT 1;
ALTER TABLE `user` AUTO_INCREMENT = 1;

use `mycoursework01`;
drop table `directory`;
drop table `file`;
CREATE TABLE IF NOT EXISTS `mycoursework01`.`file` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `file_name` VARCHAR(45) NOT NULL,
  `file_uri` VARCHAR(255) NOT NULL,
  `file_direction` VARCHAR(200),
  `user_id` INT(11) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_file_user_idx` (`user_id` ASC) VISIBLE,
  CONSTRAINT `fk_file_user`
    FOREIGN KEY (`user_id`)
    REFERENCES `mycoursework01`.`user` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

ALTER TABLE `file` ALTER `id` SET DEFAULT 1;
ALTER TABLE `file` AUTO_INCREMENT = 1;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;



